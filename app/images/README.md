These assets have been optimized with pngquant achieving an additional 60-80% file size savings over their source images.

AmazonUICarousel-arrows is a snapshot of the sprite used on amazon.com's hero, but lives in the app to prevent the buttons from breaking if the image changes.