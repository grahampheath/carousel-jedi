'use strict';

chrome.runtime.onInstalled.addListener(details => {
  console.log('previousVersion', details.previousVersion);
});

chrome.tabs.onUpdated.addListener(tabId => {
  chrome.pageAction.show(tabId);
});

console.log('This file really is here only to enable the icon coloring.');
console.log('Seriously, I tried removing it, and the icon went gray!');
