// Redux Reducers see; http://redux.js.org/docs/basics/Reducers.html

const defaultState = {
  items: [],
  itemsDirty: false
};

// This object contains 100% of the state manipulation for the application,
// its rather brief.
const reducerAction = {
  '@@redux/INIT': (state, action) => {
    return state;
  },
  'set': (state, action) => {
    return Object.assign({}, state, {
      items: action.items,
      itemsDirty: true
    });
  },
  'clean': (state, action) => {
    return Object.assign({}, state, {
      itemsDirty: false,
    });
  },
};

/**
  * Checks for the current action and calles it based on a dispatch event.
  * Throws an error when there is no match.
  */
module.exports = function(state = defaultState, action) {
  if (!action) {
    return state;
  }

  if (action.type in reducerAction) {
    state = reducerAction[action.type](state, action);
    if (typeof state == 'undefined') {
      throw Error(`Reducer failed to return a state. action: "${action}"`);
    }
  } else {
    throw Error(`action: [error: no process found for "${action.type}"]`);
  }

  return state;
};
