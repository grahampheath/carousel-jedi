/**
  * @fileoverview
  *
  * Cards template, renders wrapping dom for the carousel.
  */

const DOMTemplate = require('./util/DOMTemplate');

/** Template for rendering the user's data section. */
const TwitterUserTemplate = require('./TwitterUser');
const TwitterControlsTemplate = require('./TwitterControls');

/** Linkify converts our tweets into html with clickable links. */
const linkify = require('linkifyjs');

require('linkifyjs/plugins/hashtag')(linkify); // Support # hastags links.
require('linkifyjs/plugins/mention')(linkify); // Support @ mentions links.

const linkifyHtml = require('linkifyjs/html');

/**
  * Linkify needs to be told where the links go.
  *
  * TODO: Linkify doesn't support full UTF-8 apparently, some links are broken
  * at accented characters.
  */
const linkifyConfig = {
  defaultProtocol: 'https',
  formatHref: {
    hashtag: (val) => 'https://twitter.com/hashtag/' + val.substr(1),
    mention: (val) => 'https://twitter.com' + val
  }
};

/** Renders the Twitter card template rendering. */
module.exports = function(keys) {
  if (keys.loading) {
    keys.text = 'loading...';
  }

  if (keys.text) {
    // Convert links, #s. and @s to links.
    keys.text = linkifyHtml(keys.text, linkifyConfig);
  }

  let card = DOMTemplate`<li class="azcr-siema__card">
      <p class="azcr-siema__card__text">${'text'}</p>
    </li>`(keys);

  if (keys.user) {
    // Add the users data to the top of the card.
    card.prepend(TwitterUserTemplate(keys.user));
  }

  if (!keys.loading) {
    // Add the like, retweet, and view on twitter buttons.
    card.append(TwitterControlsTemplate(keys));
  }

  return card;
};