/**
  * ES6 Template literal shortcut method for interpolating Objects<string>s into
  * the expressions.
  *
  * The parameter `strings`
  *
  * @param {Array<strings>} strings Template literal fragments.
  * @param {Array<strings>} keys The remaining args are the keys to be replaced.
  *
  * Largely from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
  * with an addition of a parser to create DOM nodes instead of strings.
  */
const DOMTemplate = function(strings, ...keys) {
  // Parser for converting the rendered template into DOM nodes.
  const parser = new DOMParser();

  return (function(...values) {
    var dict = values[values.length - 1] || {};
    var result = [strings[0]];
    keys.forEach(function(key, i) {
      var value = Number.isInteger(key) ? values[key] : dict[key];
      result.push(value, strings[i + 1]);
    });

    // This line is changed to parse the template to HTML.
    return parser.parseFromString(result.join(''), 'text/html').body.firstChild;
  });
};
module.exports = DOMTemplate;