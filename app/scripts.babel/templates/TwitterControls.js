/**
  * @fileoverview
  *
  * Renders twitter buttons for cards.
  */

const DOMTemplate = require('./util/DOMTemplate');

module.exports = function(keys) {
  return DOMTemplate`<p class="azcr-siema__card__icons">
  <a
      target="_blank"
      class="azcr-siema__card__icons--retweet"
      href="https://twitter.com/intent/retweet?tweet_id=${'twitterID'}">
    <span>Retweet</span>
  </a>
  <a
      target="_blank"
      class="azcr-siema__card__icons--like"
      href="https://twitter.com/intent/like?tweet_id=${'twitterID'}">
    <span>Like</span>
  </a>
  <a
      target="_blank"
      class="azcr-siema__card__icons--twitter"
      href="${'link'}">
    <span>View on Twitter</span>
  </a>
</p>`(keys);
};
