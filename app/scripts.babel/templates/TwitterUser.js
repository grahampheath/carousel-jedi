/**
  * @fileoverview
  *
  * Twitter user template renders the image, name, and screen name for the
  * user.
  */
const DOMTemplate = require('./util/DOMTemplate');

/** @enum */
const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'June',
  'July',
  'Aug',
  'Sept',
  'Oct',
  'Nov',
  'Dec',
];

/** Renders the Twitter user data template. */
module.exports = function(keys) {
  // Format the date like Twitter; "1 Jan".
  let date = new Date(Date.parse(keys.created_at));
  keys.date = `${date.getMonth()} ${months[date.getDay()]}`;

  return DOMTemplate`
<div class="azcr-siema__user">
  <a href="${'url'}" target="_blank">
    <p>
      <img class="azcr-siema__user-image" src="https://twitter.com/${'screen_name'}/profile_image?size=normal"/>
    </p>
    <p>
      <b>${'name'}</b>
      <br/>
      @${'screen_name'} • ${'date'}
    </p>
  </a>
</div>
`(keys);
};