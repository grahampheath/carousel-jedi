/**
  * @fileoverview
  *
  * Cards template, renders wrapping dom for the carousel.
  */

const DOMTemplate = require('./util/DOMTemplate');

module.exports = function(keys = {}) {
  /**
    * I've mentioned earlier that the template literals appear a bit clunky,
    * here's a good example: adding a helpers to look up images was
    * impractical so I'm adding the data with js. However, I do approve of
    * the seperation of rendering and look up, so its ¯\_(ツ)_/¯ worthy.
    */
  keys.watermarkIcon = chrome.runtime.getURL('images/icon-38.png');
  keys.navigationArrowsSprite =
  chrome.runtime.getURL('images/AmazonUICarousel-arrows.png');

  return DOMTemplate`
<div>
  <hr class="slot-hr desktop-sidekick-1-and-one-half-hr">
  <div class="azcr-siema__wrapper">
    <div class="azcr-siema__direction-nav">
      <img
          class="azcr-siema__watermark"
          src="${'watermarkIcon'}"
          alt="AZCR Brought to you by Graham P Heath."/>
      <button
          class="azcr-siema__prev"
          style="background-image:url(${'navigationArrowsSprite'})"
          aria-label="Previous Tweet">
      </button>
      <button
          class="azcr-siema__next"
          style="background-image:url(${'navigationArrowsSprite'})"
          aria-label="Next Tweet">
      </button>
    </div>
    <div class="azcr-siema__carousel">
      <ul>
      </ul>
    </div>
  </div>
</div>`
  (keys);
};