/**
  * @fileOverview
  *
  * This is a chrome extension's content script.
  *
  * `require()` over `import bar from foo` because we're using Browserify 😭.
  */
{
  require('./carouselJedi')();

  /** Inject my styles, I couldn't get this working with manifest.json. */
  document.head.insertAdjacentHTML('beforeend',
      '<link rel="stylesheet" type="text/css" href="' +
            chrome.runtime.getURL("styles/cards.css") + '">');

}