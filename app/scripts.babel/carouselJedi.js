/**
  * @fileOverview
  *
  * Carousel Jedi is a Redux and vanilla js injector and manager for a simple
  * twitter carousel.
  *
  * `require()` over `import bar from foo` because we're using Browserify 😭.
  */
{
  /**
    * The card slider library Siema. https://pawelgrzybek.com/siema
    *
    * Chosen for its functionality, file weight, and easy visual
    * customizability.
    *
    * Provided by npm and Browserify.
    */
  const Siema = require('siema');

  /**
    * Redux. http://redux.js.org/.
    *
    * For this usage all we need is createStore. Simply put, its a bit of a
    * state provider, and a bit of a pub/sub system.
    *
    * http://redux.js.org/docs/basics/Store.html
    *
    * Provided by npm and Browserify.
    */
  const { createStore } = require('redux');

  /**
    * ES6 Template literal templates. In the future, I'd use a production ready
    * approach, however given the context, I think some of this is pretty neat:
    * `linkify` is used in the TwitterCardTemplate to convert the tweets to
    * interactive elements, which would be worth looking at.
    */
  const CardsDOMTemplate = require('./templates/Cards');
  const TwitterCardDOMTemplate = require('./templates/TwitterCard');

  /** @enum */
  const SELECTORS = {
    insertionTarget: '#desktop-sidekick-2',
    carousel: '.azcr-siema__carousel',
    carouselNext: '.azcr-siema__next',
    carouselPrev: '.azcr-siema__prev',
  };

  /**
    * Some results have no user, these results have broken links, so i'm hiding
    * them all
    */
  const NO_USER_WARNING = 'This tweet has been dropped because it lacked a ' +
      'user property';

  /**
    * There is a `gulp fetch` task that pulls the api and saves it to a local
    * json that can be used with require('../data/results.json'), instead of
    * this line.
    *
    * I decided to abandon the full on preloading because nothing changed on the
    * page when the fetched content was rendered. This preloading proved nice in
    * development, as well as for creating the "loading" indicator.
    *
    * @type {Object} A simple pseudo reply from the "Twitter API" json that has
    * the word "loading..." in place of a tweet.
    */
  const TwitterFeedData = require('../data/loading.json');

  module.exports = function() {
    /**
      * Redux is great for interactions. Although this experience is very
      * simple, seperating state and rendering is still good practice.
      *
      * The store will contain our state, and alert any subscribers of changes.
      *
      * @type {Object<function>}
      */
    const store = createStore(require('./Reducers'));

    /** @type {Element} */
    const insertionTarget = document.querySelector(SELECTORS.insertionTarget);

    /**
      * @type {Object<undefined>|Siema} Siema instance, may also be an empty
      *     object before the creation of the handler or between it being
      *     destroyed and being recreated.
      */
    let mySiema = {};

    /** @type {function()} Next handler, named for additon and removal. */
    const next = () => {
      mySiema.next();
    };

    /** @type {function()} Previous handler, named for additon and removal. */
    const prev = () => {
      mySiema.prev();
    };


    /**
      * @type {function()} Removes all dom listeners caused by creating a
      *     mySiema instance.
      */
    const destroySiema = () => {
      while (carousel.firstChild) {
        carousel.removeChild(carousel.firstChild);
      }

      if ('destroy' in mySiema) {
        document.querySelector(SELECTORS.carouselPrev)
            .removeEventListener('click', prev);
        document.querySelector(SELECTORS.carouselNext)
            .removeEventListener('click', next);
        mySiema.destroy();
      }
    };

    /**
      * @type {function()} Restores all dom listeners caused by for a mySiema
      *     instance.
      * @return {Siema} New Semia instance, complete with dom listeners
      *     and new items.
      */
    const createSiema = (state) => {
      state.items.map((item) => {
        if (item.loading || item.user) {
          carousel.appendChild(TwitterCardDOMTemplate(item));
        } else {
          console.warn(NO_USER_WARNING, item);
        }
      });

      document.querySelector(SELECTORS.carouselPrev)
          .addEventListener('click', prev);
      document.querySelector(SELECTORS.carouselNext)
          .addEventListener('click', next);

      // After experimentation this the easing I decided on:
      // http://cubic-bezier.com/#.21,.32,.39,1
      return new Siema({
        easing: 'cubic-bezier(.21,.32,.39,1)',
        selector: SELECTORS.carousel
      });
    };

    /**
      * Renders the current state, exactly the same way everytime the state is
      * updated.
      *
      * From there, the carousel component handles the interaction.
      *
      * Given the limited number of tweets we're showing at time I took a more
      * aggressive approach and I destroy and re-created the  carousel on any
      * change. This was a concious decision early on, and was the primary
      * reason I did not feel the requirement for a framework.
      *
      * TODO: Update only changed items.
      */
    const render = function() {
      const state = store.getState();
      // Dirty would mean there are new items that need to be rendered.
      if (state.itemsDirty) {
        destroySiema(mySiema);
        mySiema = createSiema(state);
        store.dispatch({type:'clean'});
      }
    };

    /**
      * Add our wrapper elements.
      */
    insertionTarget.insertBefore(CardsDOMTemplate(), null);

    let carousel = insertionTarget.querySelector(SELECTORS.carousel);

    let unsubscribe = store.subscribe(render);

    // Remember that at this point, this is simply a loading flag.
    store.dispatch({
      type: 'set',
      items: TwitterFeedData
    });

    // Using native fetch.
    fetch('https://houdini-api.firebaseio.com/results.json').then((response) => {
      // Assemble the chunks as json.
      return response.json();
    }).then((response) => {
      // Let the store know that there's new data.
      store.dispatch({
        type: 'set',
        items: response,
      });
    });
  };
}