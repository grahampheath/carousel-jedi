# AzCR


Is a Chrome Extension that injects the results of an API call on to www.amazon.com (or smile.amazon.com).

The demo JSON seems to have grown stale with some tweets lacking user information, and another with a user's profile (including pic) 404'ing. The ones lacking user data are simply dropped, but it was not practical to filter users based on if the user exists.

This project leverages Browserify and Babel on the back end to bundle `npm` resources in with frontend code.

The source javascript files are located in /app/scripts.babel, with /app/scripts being a temp folder created by Babel.

#run locally
Install with `npm`.

Build with `gulp build`.

There's also `gulp watch` (build on change), and  `gulp package` (./dist -> azcr.zip).

[Chrome Reloader](https://chrome.google.com/webstore/detail/extensions-reloader/fimgfedafeadlieiabdeeaodndnlbhid) is useful as well.
