import path from 'path';

import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';

import resolve from 'rollup-plugin-node-resolve';

import babelify from 'babelify';
import browserify from 'browserify';
import rollupify from 'rollupify';
import watchify from 'watchify';

import globby from 'globby';
import through from 'through2';

import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import gutil from 'gulp-util';

import del from 'del';
import runSequence from 'run-sequence';
import {stream as wiredep} from 'wiredep';
import request from 'request';
import fs from 'fs';

import openurl from 'openurl';

const $ = gulpLoadPlugins();

gulp.task('extras', () => {
  return gulp.src([
    'app/*.*',
    'app/_locales/**',
    '!app/scripts.babel',
    '!app/*.json',
    '!app/*.html',
    '!app/styles.scss'
  ], {
    base: 'app',
    dot: true
  }).pipe(gulp.dest('dist'));
});

function lint(files, options) {
  return () => {
    return gulp.src(files)
      .pipe($.eslint(options))
      .pipe($.eslint.format());
  };
}

gulp.task('lint', lint('app/scripts.babel/**/*.js', {
  env: {
    es6: true
  }
}));

gulp.task('images', () => {
  return gulp.src('app/images/**/*')
    .pipe($.if($.if.isFile, $.cache($.imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{cleanupIDs: false}]
    }))
    .on('error', function (err) {
      console.log(err);
      this.end();
    })))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('styles', () => {
  return gulp.src('app/styles.scss/*.scss')
    .pipe($.plumber())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    })
    .on('error', $.sass.logError))
    .pipe(gulp.dest('app/styles'));
});

gulp.task('html', ['styles'], () => {
  return gulp.src('app/*.html')
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.sourcemaps.init())
    // .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.cleanCss({compatibility: '*'})))
    .pipe($.sourcemaps.write())
    .pipe($.if('*.html', $.htmlmin({removeComments: true, collapseWhitespace: true})))
    .pipe(gulp.dest('dist'));
});

gulp.task('chromeManifest', () => {
  return gulp.src('app/manifest.json')
    .pipe($.chromeManifest({
      buildnumber: true
    }))
    .pipe($.if('*.css', $.cleanCss({compatibility: '*'})))
    .pipe($.if('*.js', $.sourcemaps.init()))
    // .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.js', $.sourcemaps.write('.')))
    .pipe(gulp.dest('dist'));
});

gulp.task('babel', () => {
  // Disable reading because browserify reads.
  return gulp.src('app/scripts.babel/**/*.js', {read: false})
    // Transforms the file objects using gulp-tap plugin.
    .pipe($.tap(function (file) {
      const entry = './app/scripts.babel/' + path.relative(__dirname, file.path).replace('app/scripts.babel/','').replace('.tmp','');

      gutil.log('bundling ' + JSON.stringify(entry, null, 2));
      // Replaces file contents with browserify's bundle stream.
      file.contents = browserify(entry, {
          debug: true,
          cache: {},
          packageCache: {}
        })
        // .transform('rollupify', {
        //   plugins: [
        //     resolve({
        //       jsnext: true,
        //       module: true,
        //       browser: true,
        //       customResolveOptions: {
        //         moduleDirectory: 'node_modules'
        //       }
        //     }),
        //   ]
        // })
        .transform('babelify')
        .bundle().on('error', function (err) {
          gutil.log('Browserify / Rollup:', err.toString());
          this.emit("end");
        });
    }))

    // Transform streaming contents into buffer contents
    // (because gulp-sourcemaps does not support streaming contents).
    .pipe(buffer())

    // Loads and inits sourcemaps.
    .pipe($.sourcemaps.init({loadMaps: true}))

    // .pipe($.uglify())

    // Write sourcemaps.
    .pipe($.sourcemaps.write('./'))

    .pipe(gulp.dest('app/scripts'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('watch', () => {
  $.livereload.listen();

  gulp.watch([
    'app/*.html',
    'app/scripts/**/*.js',
    'app/images/**/*',
    'app/styles/**/*',
    'app/_locales/**/*.json'
  ], ['build']);

  gulp.watch('app/scripts.babel/**/*.js', ['lint', 'babel']);
  gulp.watch('app/styles.scss/**/*.scss', ['styles']);
  gulp.watch('bower.json', ['wiredep']);
});

gulp.task('fetch', () => {
  request('https://houdini-api.firebaseio.com/results.json')
    .pipe(fs.createWriteStream('app/data/results.json'));
});

gulp.task('size', () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('wiredep', () => {
  gulp.src('app/*.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('package', function () {
  var manifest = require('./dist/manifest.json');
  return gulp.src('dist/**')
      .pipe($.zip('azcr-' + manifest.version + '.zip'))
      .pipe(gulp.dest('package'));
});

gulp.task('build', (cb) => {
  runSequence(
    'lint', 'babel', 'chromeManifest', 'styles',
    ['html', 'images', 'extras'],
    'size', cb);
});

gulp.task('default', ['clean'], cb => {
  runSequence('build', cb);
});
